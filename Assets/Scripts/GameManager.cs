﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public int _balls = 3;
    public int _bricks = 22;
    public float _resetDelay = 1f;
    public Text _ballsLeft;
    public Text _scoreText;
    public GameObject _gameOver;
    public GameObject _youWon;
    public GameObject _bricksPrefab;
    public GameObject _paddle;
    public GameObject _deathParticles;

    public static GameManager _instance = null;

    private AudioSource _audioSource;
    private GameObject _clonePaddle;
    private GameObject _ball;
    private int _score = 0;
    private int _blockPoints = 100;
    private int _lifePoints = 500;
    private int _powerUpPoints = 200;


    // Use this for initialization
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        _audioSource = GetComponent<AudioSource>();
        _ball = GetComponent<GameObject>();

        Setup();
    }

    public void Setup()
    {
        _clonePaddle = Instantiate(_paddle, transform.position, Quaternion.identity) as GameObject;
        Instantiate(_bricksPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }

    /// <summary>
    /// If bricks are less than one display win message.
    /// If lives are less than one display game over message.
    /// </summary>
    void CheckGameOver()
    {
        if (_bricks < 1)
        {
            _score += _lifePoints * _balls;
            _scoreText.text = "Score: " + _score;
            _youWon.SetActive(true);
            Time.timeScale = .25f;
            Invoke("Reset", _resetDelay);
        }

        if (_balls < 1)
        {
            _gameOver.SetActive(true);
            Time.timeScale = .25f;
            Invoke("Reset", _resetDelay);
        }
    }

    /// <summary>
    /// Start a new level.
    /// </summary>
    void Reset()
    {
        Time.timeScale = 1f;
        Application.LoadLevel(Application.loadedLevel);
    }

    /// <summary>
    /// Subtract a life and update life text. Also, destroy the paddle and create a new one. 
    /// Then check for game over.
    /// </summary>
    public void LoseLife()
    {
        _balls--;
        _ballsLeft.text = "Balls: " + _balls;
        Instantiate(_deathParticles, _clonePaddle.transform.position, Quaternion.identity);
        _audioSource.Play();
        Destroy(_clonePaddle);
        DestroyBall();
        Invoke("SetupPaddle", _resetDelay);
        CheckGameOver();
    }

    /// <summary>
    /// Instantiate a new paddle.
    /// </summary>
    void SetupPaddle()
    {
        _clonePaddle = Instantiate(_paddle, transform.position, Quaternion.identity) as GameObject;
    }

    /// <summary>
    /// Subtract a brick from the total for bricks and check if player won.
    /// </summary>
    public void DestroyBrick()
    {
        _bricks--;
        _score += _blockPoints;
        _scoreText.text = "Score: " + _score;
        CheckGameOver();

    }

    /// <summary>
    /// Destroy ball GameObject.
    /// </summary>
    public void DestroyBall()
    {
        _ball = GameObject.FindGameObjectWithTag("Ball");
        Destroy(_ball);
    }

    /// <summary>
    /// Controls if points or life will be added.
    /// </summary>
    /// <param name="powerUp"></param>
    public void TriggerPowerUp(GameObject powerUp)
    {
        if (powerUp.gameObject.CompareTag("PowerUpGrowPaddle"))
        {
            _score += _powerUpPoints;
            _scoreText.text = "Score: " + _score;
        }

        if (powerUp.gameObject.CompareTag("PowerUpShrinkPaddle"))
        {
            _score -= _powerUpPoints;
            _scoreText.text = "Score: " + _score;
        }

        if (powerUp.gameObject.CompareTag("PowerUpExtraLife"))
        {
            _balls++;
            _ballsLeft.text = "Balls: " + _balls;
        }

    }
}
