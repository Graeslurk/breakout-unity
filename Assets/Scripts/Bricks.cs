﻿using UnityEngine;
using System.Collections.Generic;

public class Bricks : MonoBehaviour {

    public GameObject _brickParticle;
    private GameObject _powerUp;
    public List<GameObject> _powerUpList;

    void Awake()
    {
        // Maybe move to OnCollision for performance?
        if (_powerUpList.Count > 0)
        {
            // Randomize which power-up the brick will contain
            _powerUp = _powerUpList[Random.Range(0, _powerUpList.Count)];
        }
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.CompareTag("Ball"))
        {
            Instantiate(_brickParticle, transform.position, Quaternion.identity);
            GameManager._instance.DestroyBrick();

            // Have a chance to spawn a power up
            float rand = Random.Range(0, 100);
            if (rand > 0)
            {
                Instantiate(_powerUp, transform.position, Quaternion.Euler(0, 0, 90));
            }

            Destroy(gameObject);
        }

    }
}
