﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	public float _paddleSpeed = 4f;

	private float _yPos = -90f;
	private float _xBoundary = 117f;
	private Vector3 _paddlePosition = new Vector3(0, -90f, 0);
	private AudioSource _audioSource;
	private Vector3 _paddleScaler = new Vector3(10f, 0, 0);

	void Awake()
	{
		_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float xPos = transform.position.x + (Input.GetAxis("Mouse X") * _paddleSpeed);
		_paddlePosition = new Vector3(Mathf.Clamp(xPos, -_xBoundary, _xBoundary), _yPos, 0);
		transform.position = _paddlePosition;
	}

	void OnCollisionEnter()
	{
		_audioSource.Play();
	}

	void OnTriggerEnter(Collider other)
	{
		GameManager._instance.TriggerPowerUp(other.gameObject);

		if (other.gameObject.CompareTag("PowerUpGrowPaddle"))
		{
			Destroy(other.gameObject);
			transform.localScale += _paddleScaler;
			_xBoundary -= 5;

		}

		if (other.gameObject.CompareTag("PowerUpShrinkPaddle"))
		{
			Destroy(other.gameObject);

			// To prevent negative x scaling
			if (transform.localScale.x > _paddleScaler.x)
			{
				transform.localScale += -_paddleScaler;
				_xBoundary += 5;
			}
		}

		if (other.gameObject.CompareTag("PowerUpExtraLife"))
		{
			Destroy(other.gameObject);
		}
	}
}
