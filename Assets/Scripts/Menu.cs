﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public Canvas _exitMenu;
	public Button _playButton;
	public Button _exitButton;

	// Use this for initialization
	void Start ()
	{
		_exitMenu = _exitMenu.GetComponent<Canvas>();
		_playButton = _playButton.GetComponent<Button>();
		_exitButton = _exitButton.GetComponent<Button>();

		_exitMenu.enabled = false;
	}

	public void PlayPress()
	{
		Application.LoadLevel(1);
	}

	public void ExitPress()
	{
		_exitMenu.enabled = true;
		_playButton.enabled = false;
		_exitButton.enabled = false;
	}

	public void NoPress()
	{
		_exitMenu.enabled = false;
		_playButton.enabled = true;
		_exitButton.enabled = true;
	}

	public void YesPress()
	{
		Application.Quit();
	}


}
