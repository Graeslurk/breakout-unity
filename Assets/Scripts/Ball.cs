﻿using UnityEngine;

public class Ball : MonoBehaviour {

	public float _ballInitVelocity = 3000f;

	private Rigidbody _rb;
	private bool _ballInPlay;
	private float _paddleBounce = 100f;
	private AudioSource _audioSource;
	private float volLow = 0.2f;
	private float volHigh = 0.5f;

	void Awake()
	{
		_rb = GetComponent<Rigidbody>();
		_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1") && _ballInPlay == false)
		{
			// "unparent" the ball from the paddle so it can fly away
			transform.parent = null;
			_ballInPlay = true;
			_rb.isKinematic = false;
			// set a force on the ball so it flies away when you press ctrl or LMB
			_rb.AddForce(new Vector3(_ballInitVelocity, _ballInitVelocity, 0));
		}
	}

	void CollideWithPaddle(Collision coll)
	{
		Vector3 paddlePosition = coll.transform.position;
		Vector3 ballPosistion = gameObject.transform.position;
		
		Vector3 direction = (ballPosistion - paddlePosition).normalized;
		_rb.velocity = direction * _paddleBounce;
	}

	void OnCollisionEnter(Collision coll)
	{
		float vol = Random.Range(volLow, volHigh);
		_audioSource.volume = vol;

		if (coll.collider.CompareTag("Paddle"))
		{
			CollideWithPaddle(coll);
		}

		if (coll.collider.CompareTag("Wall"))
		{
			_audioSource.Play();
		}

		if (coll.collider.CompareTag("Brick"))
		{
			_audioSource.Play();
		}
	}
}
