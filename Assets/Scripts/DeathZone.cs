﻿using UnityEngine;

public class DeathZone : MonoBehaviour {

    /// <summary>
    /// When hitting the void - lose a life.
    /// </summary>
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUpGrowPaddle") ||
            other.gameObject.CompareTag("PowerUpShrinkPaddle") ||
            other.gameObject.CompareTag("PowerUpExtraLife"))
        {
            Destroy(other.gameObject);
        }
        else
            GameManager._instance.LoseLife();

    }
    
}
